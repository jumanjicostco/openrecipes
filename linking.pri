!win32 {
	LIBS += -L$$PWD/lib -lopenrecipes
}

win32 {
	CONFIG(release, debug|release): LIBS += -L$$PWD/lib/release -lopenrecipes
	CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/debug -lopenrecipes
}

INCLUDEPATH += $$PWD/lib/src
