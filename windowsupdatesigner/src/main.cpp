/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QCommandLineParser>
#include <QFile>
#include <QtDebug>
#include <config.h>
#include <fstream>
#include <sodium.h>

static bool generateKey(const QString &output) {
	QFile file(output);
	if (file.exists()) {
		qCritical() << "File" << output << "allready exists, will not override";
		return false;
	} else {
		file.open(QIODevice::NewOnly | QIODevice::WriteOnly);
	}

	qInfo() << "Generating a new key...";
	unsigned char pk[crypto_sign_PUBLICKEYBYTES];
	unsigned char sk[crypto_sign_SECRETKEYBYTES];
	crypto_sign_keypair(pk, sk);

	QByteArray p = QByteArray::fromRawData(reinterpret_cast<char*>(pk), sizeof(pk));
	QByteArray s = QByteArray::fromRawData(reinterpret_cast<char*>(sk), sizeof(sk));
	if (file.write(s.toHex()) == -1) {
		qCritical() << "Can't write to file" << output;
		return false;
	}
	file.close();
	qInfo() << "Secret key saved to" << output;
	qInfo() << "The public key is" << p.toHex();

	return true;
}

static bool signFile(const QString &sign, const QString &keyFilePath, const QString &output) {
	QFile outputFile(output);
	if (outputFile.exists()) {
		qCritical() << "File" << output << "allready exists, will not override";
		return false;
	} else {
		outputFile.open(QIODevice::NewOnly | QIODevice::WriteOnly);
	}

	std::ifstream signFile(sign.toStdString(), std::ios::in | std::ios::binary);
	unsigned char chunk[1024];
	crypto_sign_state state; 
	crypto_sign_init(&state);
	while (signFile.good()) {
		signFile.read(reinterpret_cast<char*>(chunk), sizeof(chunk));
		crypto_sign_update(&state, chunk, signFile.gcount());
	}

	QFile keyFile(keyFilePath);
	keyFile.open(QIODevice::ExistingOnly | QIODevice::ReadOnly);
	const QByteArray key = QByteArray::fromHex(keyFile.readAll());
	assert(static_cast<size_t>(key.size()) >= crypto_sign_SECRETKEYBYTES);

	unsigned char sig[crypto_sign_BYTES];
	crypto_sign_final_create(&state, sig, nullptr, reinterpret_cast<const unsigned char*>(key.constData()));

	QByteArray s = QByteArray::fromRawData(reinterpret_cast<char*>(sig), sizeof(sig));
	if (outputFile.write(s.toHex()) == -1) {
		qCritical() << "Can't write to file" << output;
		return false;
	}
	outputFile.close();
	qInfo() << "Signature written to" << output;
	return true;
}

int main(int argc, char *argv[]) {
	QCoreApplication app(argc, argv);
	app.setApplicationName(QString("WindowsUpdateSigner%1").arg(config::namepostfix));
	app.setApplicationVersion(config::version);

	QCommandLineParser parser;
	parser.setApplicationDescription("Small utility to sign the windows updates");
	parser.addHelpOption();
	parser.addVersionOption();
	QCommandLineOption genKey({"g", "generate-key"}, "Generate a new key", "output file");
	parser.addOption(genKey);
	QCommandLineOption keyfile({"k", "key"}, "Key for signing", "key file");
	parser.addOption(keyfile);
	QCommandLineOption sign({"s", "sign"}, "File to sign", "file");
	parser.addOption(sign);
	QCommandLineOption outputFile({"o", "output"}, "File to save signature in", "ouput file");
	parser.addOption(outputFile);

	parser.process(app);
	
	if (sodium_init() == -1) {
		qCritical() << "Can't initialize libsodium";
		return EXIT_FAILURE;
	}

	bool ok = true;

	if (parser.isSet(genKey)) ok = ok && generateKey(parser.value(genKey));
	if (parser.isSet(sign) || parser.isSet(keyfile) || parser.isSet(outputFile)) {
		if (!parser.isSet(sign) || !parser.isSet(keyfile) || !parser.isSet(outputFile)) {
			qCritical() << "You need to sprecify all of --key, --sign and --output to sign a file";
		} else {
			ok = ok && signFile(parser.value(sign), parser.value(keyfile), parser.value(outputFile));
		}
	}

	return (ok ? EXIT_SUCCESS : EXIT_FAILURE);
}
