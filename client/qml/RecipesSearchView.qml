/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import org.jschwab.recipes 1.0

Page {
	id: recipesSearchView

	header: ToolBar {
		RowLayout {
			anchors.fill: parent

			BigLabel {
				text: qsTr("Download shared recipe")
				horizontalAlignment: Qt.AlignHCenter
				verticalAlignment: Qt.AlignVCenter
				Layout.fillWidth: true
			}
		}
	}

	ColumnLayout {
		anchors.fill: parent

		MediumLabel {
			Layout.maximumWidth: recipesSearchView.width
			Layout.alignment: Qt.AlignHCenter
			text: qsTr("Select a recipe to download:")
			wrapMode: Text.Wrap
			Layout.topMargin: 5
		}

		ListView {
			id: foundRecipes
			Layout.maximumWidth: recipesSearchView.width
			Layout.fillHeight: true
			Layout.fillWidth: true
			Layout.alignment: Qt.AlignHCenter
			orientation: ListView.Vertical
			model: backend.foundRecipesList
			ScrollBar.vertical: ScrollBar {}
			boundsBehavior: Flickable.StopAtBounds
			delegate: Item {
				width: parent.width
				height: name.height + 10
				Text {
					id: name
					font.pointSize: 15
					anchors.centerIn: parent
					text: modelData.name
					wrapMode: Text.Wrap
				}
				Rectangle {
					height: parent.width
					width: 1
					rotation: 90
					anchors.verticalCenter: parent.bottom
					anchors.horizontalCenter: parent.horizontalCenter
					gradient: Gradient {
						GradientStop{
							position: 0.0
							color: "transparent"
						}
						GradientStop{
							position: 0.5
							color: (index == foundRecipes.count - 1) ? "transparent" : Globals.accentColor
						}
						GradientStop{
							position: 1.0
							color: "transparent"
						}
					}
				}
				MouseArea {
					anchors.fill: parent
					onClicked: function() {
						backend.getRecipeFromServer(modelData.id);
						stackView.pop();
					}
				}
			}
		}

		Label {
			text: qsTr("Searching…")
			Layout.alignment: Qt.AlignHCenter
		}

		ProgressBar {
			indeterminate: true
			Layout.alignment: Qt.AlignHCenter
			Layout.bottomMargin: Globals.mobile ? 5 : 0
		}	
	}

	footer: RowLayout {
		Button {
			text: qsTr("Cancel")
			Layout.alignment: Qt.AlignRight
			onClicked: function () {
				backend.stopSearching();
				stackView.pop();
			}
			Layout.bottomMargin: 5
			Layout.rightMargin: 5
		}
		visible: !Globals.mobile
	}
}
