#include "Jni.h"
#include "SqlBackend.h"
#include "Recipe.h"

#include <QAndroidJniObject>
#include <QCoreApplication>

JNIEXPORT jboolean JNICALL Java_org_jschwab_openrecipes_Helpers_importRecipe(JNIEnv *, jclass, jstring recipeXml) {
	QAndroidJniObject xml = QAndroidJniObject::fromLocalRef(recipeXml);
	try {
		if (QCoreApplication::startingUp()) {
			Jni::importBufferMutex.lock();
			Jni::importBuffer.push_front(xml.toString());
			Jni::importBufferMutex.unlock();
		} else {
			qInfo() << "Importing recipes";
			Recipe *r = new Recipe(xml.toString(), nullptr);
			emit Jni::getNotifier()->recipeImported(r->getId());
			delete r;
		}
	} catch (const InternalError &e) {
		return false;
	}
	return true;
}

JNIEXPORT jstring JNICALL Java_org_jschwab_openrecipes_Helpers_getRecipeXml(JNIEnv *env, jclass, jstring recipeIdHex) {
	if (QCoreApplication::startingUp())	return reinterpret_cast<jstring>(env->NewLocalRef(QAndroidJniObject::fromString("").object<jstring>()));
	QAndroidJniObject idHex = QAndroidJniObject::fromLocalRef(recipeIdHex);
	try {
		Recipe *r = SqlBackend::getRecipe(QByteArray::fromHex(idHex.toString().toLatin1()));
		QAndroidJniObject xml = QAndroidJniObject::fromString(r->toXML());
		delete r;
		return reinterpret_cast<jstring>(env->NewLocalRef(xml.object<jstring>()));
	} catch (const SqlNoResult &e) {
		return reinterpret_cast<jstring>(env->NewLocalRef(QAndroidJniObject::fromString("").object<jstring>()));
	}
}

JNIEXPORT jstring JNICALL Java_org_jschwab_openrecipes_Helpers_getRecipeName(JNIEnv *env, jclass, jstring recipeIdHex) {
	if (QCoreApplication::startingUp())	return reinterpret_cast<jstring>(env->NewLocalRef(QAndroidJniObject::fromString("").object<jstring>()));
	QAndroidJniObject idHex = QAndroidJniObject::fromLocalRef(recipeIdHex);
	try {
		QString name = SqlBackend::getNameForRecipe(QByteArray::fromHex(idHex.toString().toLatin1()));
		return reinterpret_cast<jstring>(env->NewLocalRef(QAndroidJniObject::fromString(name).object<jstring>()));
	} catch (const SqlNoResult &e) {
		return reinterpret_cast<jstring>(env->NewLocalRef(QAndroidJniObject::fromString("").object<jstring>()));
	}
}

std::shared_ptr<JniNotifier> Jni::getNotifier() {
	if (!notifier) notifier = std::make_shared<JniNotifier>();
	return notifier;
}

std::forward_list<QString> Jni::importBuffer;
QMutex Jni::importBufferMutex;
std::shared_ptr<JniNotifier> Jni::notifier;
