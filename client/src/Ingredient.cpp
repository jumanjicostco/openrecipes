/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Ingredient.h"
#include "SqlBackend.h"

#include <cstring>
#include <cassert>
#include <QtEndian>

Ingredient::Ingredient(int id, QObject *parent)
:
	QObject(parent),
	id(id),
	multiplier(1)
{}

int Ingredient::getId() const {
	return id;
}

void Ingredient::setMultiplier(double multiplier) {
	this->multiplier = multiplier;
	emit multipliedCountChanged();
}

double Ingredient::getCount() const {
	try {
		return static_cast<double>(SqlBackend::getCountForIngredient(id)) / 1000;
	} catch (const SqlNoResult &e) {
		return 0;
	}
}

double Ingredient::getMultipliedCount() const {
	try {
		return getCount() * multiplier;
	} catch (const SqlNoResult &e) {
		return 0;
	}
}

QString Ingredient::getUnit() const {
	try {
		return SqlBackend::getUnitForIngredient(id);
	} catch (const SqlNoResult &e) {
		return QString();
	}
}

QString Ingredient::getArticle() const {
	try {
		return SqlBackend::getArticleForIngredient(id);
	} catch (const SqlNoResult &e) {
		return QString();
	}
}

void Ingredient::setCount(double count) {
	SqlBackend::setCountForIngredient(id, static_cast<quint64>(count * 1000));
	emit multipliedCountChanged();
}

void Ingredient::setUnit(const QString &unit) {
	SqlBackend::setUnitForIngredient(id, unit);
}

void Ingredient::setArticle(const QString &article) {
	SqlBackend::setArticleForIngredient(id, article);
}

void Ingredient::onIngredientChanged(int id) {
	if (id == -1 || this->id == id) emit dataChanged();
}
