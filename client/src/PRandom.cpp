/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PRandom.h"

#include <QByteArray>
#include <cstring>
#include <cassert>
#include <sodium.h>

QByteArray PRandom::getId() {
	if (!isSeeded) {
		std::mt19937_64::result_type seed;
		randombytes_buf(reinterpret_cast<unsigned char*>(&seed), sizeof(seed));
		generator.seed(seed);
		isSeeded = true;
	}

	QByteArray id;
	id.resize(16);
	for (size_t i = 0; i < 2; ++i) {
		const std::mt19937_64::result_type rand = generator();
		std::memcpy(id.data() + i * 8, &rand, 8);
	}
	
	return id;
}

bool PRandom::isSeeded = false;
std::mt19937_64 PRandom::generator;
