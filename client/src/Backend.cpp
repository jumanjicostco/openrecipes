/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Backend.h"
#include "SqlBackend.h"
#include "Recipe.h"
#include "SynchronizeAsync.h"
#include "SendSyncKeyAsync.h"
#include "RecvSyncKeyAsync.h"
#include <QVariant>
#include <QLocale>
#include <cassert>
#include <QDir>

#ifdef Q_OS_ANDROID
#include "Jni.h"
#include <QAndroidIntent>
#include <QtAndroid>
#include <QAndroidJniEnvironment>
#endif

#ifdef Q_OS_WIN
#include "WindowsSelfUpdate.h"
#include <QTimer>
#endif

Backend::Backend()
:
	synchronizeAsync(nullptr),
	sendSyncKeyAsync(nullptr),
	recvSyncKeyAsync(nullptr),
	recipes(SqlBackend::getRecipes())
{
	for (auto &r : recipes) r->setParent(this);
	sortRecipes();
	SqlBackend::connectSignals(this);

#ifdef Q_OS_ANDROID
	connect(Jni::getNotifier().get(), &JniNotifier::recipeImported, this, [this](const QByteArray &id) {
			emit recipeImported(id);
	});
#endif

#ifdef Q_OS_WIN
	WindowsSelfUpdate::cleanup();
	QTimer::singleShot(0, this, Backend::checkForUpdates);
#endif
}

QQmlListProperty<Recipe> Backend::getRecipesList() {
	return QQmlListProperty<Recipe>(
			this,
			this,
			[](QQmlListProperty<Recipe> *list) {
				return reinterpret_cast<Backend*>(list->data)->getRecipesCount();
			},
			[](QQmlListProperty<Recipe> *list, int pos) {
				return reinterpret_cast<Backend*>(list->data)->getRecipeAt(pos);
			}
	);
}

Recipe* Backend::getRecipe(const QByteArray &id) {
	try {
		return SqlBackend::getRecipe(id);
	} catch (const InternalError &e) {
		return nullptr;
	}
	//TODO Who frees the memory?
}

Recipe* Backend::getNewRecipe() {
	try {
		return SqlBackend::addEmptyRecipe();
	} catch (const InternalError &e) {
		return nullptr;
	}
	//TODO Who frees the memory?
}

int Backend::getRecipeListIndex(const QByteArray &id) {
	for (size_t i = 0; i < static_cast<size_t>(recipes.size()); ++i) {
		if (recipes[i]->getId() == id) return i;
	}
	qWarning() << "Can't find id in recipes";
	return 0;
}

bool Backend::deleteRecipe(Recipe *recipe) {
	try {
		SqlBackend::deleteRecipe(recipe->getId());
		return true;
	} catch (const InternalError &err) {
		return false;
	}
}

QString Backend::getFilter() const {
	return filter;
}

void Backend::setFilter(const QString &filter) {
	this->filter = filter;
	onRecipesListChanged();
}

void Backend::stopSynchronizing() {
	assert(synchronizeAsync);
	synchronizeAsync->cancel();
}

void Backend::synchronize() {
	assert(!synchronizeAsync);
	try {
		synchronizeAsync = new SynchronizeAsync(this);
	} catch (const InternalError &e) {
		emit synchronizingDone(false);
		return;
	}
	connect(synchronizeAsync, &SynchronizeAsync::done, this, [this](bool success) {
			qDebug() << "Synchronization finished " << (success ? "successfull" : "unsuccessfull");
			if (success) onRecipesListChanged();
			emit synchronizingDone(success);
			}
	);
	connect(synchronizeAsync, &SynchronizeAsync::finished, synchronizeAsync, &SynchronizeAsync::deleteLater);
	connect(synchronizeAsync, &SynchronizeAsync::finished, this, [this]() {synchronizeAsync = nullptr;});
	synchronizeAsync->start();
}

void Backend::sendSyncKey() {
	assert(!sendSyncKeyAsync);
	if (!getSyncAvailable()) {
		emit sendSyncKeyDone(false);
		return;
	}
	try {
		sendSyncKeyAsync = new SendSyncKeyAsync(this);
	} catch (const InternalError &e) {
		emit sendSyncKeyDone(false);
		return;
	}
	connect(sendSyncKeyAsync, &SendSyncKeyAsync::done, this, [this](bool success) {
			qDebug() << "SendSyncKey finished " << (success ? "successfull" : "unsuccessfull");
			emit sendSyncKeyDone(success);
			}
	);
	connect(sendSyncKeyAsync, &SendSyncKeyAsync::finished, sendSyncKeyAsync, &SendSyncKeyAsync::deleteLater);
	connect(sendSyncKeyAsync, &SendSyncKeyAsync::finished, this, [this]() {sendSyncKeyAsync = nullptr;});
	sendSyncKeyAsync->start();
}

QString Backend::getSyncKeyHex() const {
	try {
		const QByteArray tmp = SqlBackend::getSyncKey().toHex();
		QByteArray hex;
		for (size_t i = 0; i < static_cast<size_t>(tmp.size()); ++i) {
			if (i != 0 && i % 4 == 0) hex += ' ';
			hex += tmp.at(i);
		}
		return QString::fromLatin1(hex).toUpper();
	} catch (const SqlNoResult &e) {
		return QString();
	}
}

bool Backend::getUseCustomSyncServer() const {
	try {
		return SqlBackend::getUseCustomSyncServer();
	} catch (const SqlNoResult &e) {
		return false;
	}
}

QString Backend::getCustomSyncServerAddr() const {
	try {
		return SqlBackend::getCustomSyncServerAddr();
	} catch (const SqlNoResult &e) {
		return QString();
	}
}

int Backend::getCustomSyncServerPort() const {
	try {
		return SqlBackend::getCustomSyncServerPort();
	} catch (const SqlNoResult &e) {
		return 0;
	}
}

void Backend::setUseCustomSyncServer(bool use) const {
	SqlBackend::setUseCustomSyncServer(use);
}

void Backend::setCustomSyncServerAddr(const QString &addr) const {
	SqlBackend::setCustomSyncServerAddr(addr);
}

void Backend::setCustomSyncServerPort(int port) const {
	SqlBackend::setCustomSyncServerPort(port);
}

void Backend::setCustomSyncServerKeyHex(const QString &key) const {
	SqlBackend::setCustomSyncServerKey(QByteArray::fromHex(key.toLatin1()));
}

QString Backend::getCustomSyncServerKeyHex() const {
	try {
		return QString::fromLatin1(SqlBackend::getCustomSyncServerKey().toHex()).toUpper();
	} catch (const SqlNoResult &e) {
		return QString();
	}
}

void Backend::startTransaction() {
	qDebug() << "Backend::startTransaction()";
	assert(!transaction);
	transaction.reset(new SqlTransaction);
	SqlBackend::connectSignals(transaction.get());
}

void Backend::commitTransaction() {
	qDebug() << "Backend::commitTransaction()";
	assert(transaction);
	transaction->commit();
	transaction.reset();
}

void Backend::rollbackTransaction() {
	qDebug() << "Backend::rollbackTransaction()";
	assert(transaction);
	transaction.reset();
}

void Backend::onSyncSettingsChanged() {
	emit syncSettingsChanged();
}

void Backend::removeSyncSettings() const {
	SqlBackend::removeSyncSettings();
}

void Backend::onRecipesListChanged() {
	qDebug() << "Backend::onRecipesListChanged()";
	QList<Recipe*> newRecipes = SqlBackend::getRecipes(filter);
	QList<Recipe*> oldRecipes;
	oldRecipes.swap(recipes);
	for (const auto &o : oldRecipes) {
		bool del = true;
		for (const auto &n : newRecipes) {
			if (o->getId() == n->getId()) {
				recipes.push_back(o);
				newRecipes.removeOne(n);
				del = false;
				break;
			}
		}
		if (del) o->deleteLater();
	}
	recipes += newRecipes;
	sortRecipes();
	for (auto &r : recipes) r->setParent(this);
	emit recipesListChanged();
}

void Backend::sortRecipes() {
	std::sort(recipes.begin(), recipes.end(), [](const Recipe* first, const Recipe *second) {
			return QString::compare(first->getName(), second->getName(), Qt::CaseInsensitive) < 0;
	});
}

void Backend::onRecipeChanged(const QByteArray &id) {
	Q_UNUSED(id)
	qDebug() << "Backend::onRecipeChanged";
	sortRecipes();
	emit recipesListChanged();
}

Recipe* Backend::getRecipeAt(int pos) {
	return recipes.at(pos);
}

int Backend::getRecipesCount() const {
	return recipes.size();
}

bool Backend::getSyncAvailable() const {
	try {
		SqlBackend::getSyncKey();
		return true;
	} catch (const SqlNoResult &e) {
		return false;
	}
}

void Backend::setupSync() const {
	SqlBackend::setupSync();
}

void Backend::setupSyncFromKey(const QString &key) {
	assert(!recvSyncKeyAsync);
	try {
		recvSyncKeyAsync = new RecvSyncKeyAsync(this, QByteArray::fromHex(key.toLatin1()));
	} catch (const InternalError &e) {
		emit recvSyncKeyDone(false);
		return;
	}
	connect(recvSyncKeyAsync, &RecvSyncKeyAsync::done, this, [this](bool success) {
			qDebug() << "recvSyncKey finished " << (success ? "successfull" : "unsuccessfull");
			emit recvSyncKeyDone(success);
			}
	);
	connect(recvSyncKeyAsync, &RecvSyncKeyAsync::finished, recvSyncKeyAsync, &RecvSyncKeyAsync::deleteLater);
	connect(recvSyncKeyAsync, &RecvSyncKeyAsync::finished, this, [this]() {recvSyncKeyAsync = nullptr;});
	recvSyncKeyAsync->start();
}

#ifdef Q_OS_ANDROID
void Backend::handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data) {
	switch (receiverRequestCode) {
		case static_cast<int>(AndroidRequestCodes::GetImage):
			{
				if (resultCode != -1) { //android.app.Activity.RESULT_OK == -1
					qInfo() << "GetImage not successfull";
				} else {
					QAndroidJniObject uri = data.callObjectMethod("getData", "()Landroid/net/Uri;");
					QAndroidJniObject img = QAndroidJniObject::callStaticObjectMethod("org/jschwab/openrecipes/Helpers", "readImage", "(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;", uri.object(), QtAndroid::androidContext().object());
					emit openImage(img.toString());
				}
			}
			break;
		case static_cast<int>(AndroidRequestCodes::GetQRCode):
			{
				if (resultCode != -1) { //android.app.Activity.RESULT_OK == -1
					qInfo() << "GetQRCode not successfull";
				} else {
					QAndroidJniObject scanResult = QAndroidJniObject::fromString("SCAN_RESULT");
					QString qrcode = data.callObjectMethod("getStringExtra", "(Ljava/lang/String;)Ljava/lang/String;", scanResult.object<jstring>()).toString();
					setupSyncFromKey(qrcode);
				}
			}
		default:
			qDebug() << "Got unknown request code";
	}
}

void Backend::requestImage() {
	QAndroidIntent intent("android.intent.action.OPEN_DOCUMENT");

	QAndroidJniObject metaType = QAndroidJniObject::fromString("image/*");
	intent.handle().callObjectMethod("setType", "(Ljava/lang/String;)Landroid/content/Intent;", metaType.object<jstring>());

	QAndroidJniObject category = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Intent", "CATEGORY_OPENABLE");
	intent.handle().callObjectMethod("addCategory", "(Ljava/lang/String;)Landroid/content/Intent;", category.object<jstring>());

	QtAndroid::startActivity(intent.handle(), static_cast<int>(AndroidRequestCodes::GetImage), this);
	QAndroidJniEnvironment jniEnv;
	if (jniEnv->ExceptionCheck()) jniEnv->ExceptionClear();
}

void Backend::requestQRCode() {
	QAndroidIntent intent("com.google.zxing.client.android.SCAN");

	QAndroidJniObject scanMode = QAndroidJniObject::fromString("SCAN_MODE");
	QAndroidJniObject qrCodeMode = QAndroidJniObject::fromString("QR_CODE_MODE");
	intent.handle().callObjectMethod("putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;", scanMode.object<jstring>(), qrCodeMode.object<jstring>());

	QtAndroid::startActivity(intent.handle(), static_cast<int>(AndroidRequestCodes::GetQRCode), this);

	QAndroidJniEnvironment jniEnv;
	if (jniEnv->ExceptionCheck()) {
		/* The user has no fitting app, so we let him install one */
		jniEnv->ExceptionClear();
		QAndroidJniObject uriString = QAndroidJniObject::fromString("market://details?id=com.google.zxing.client.android");
		QAndroidJniObject uri = QAndroidJniObject::callStaticObjectMethod("android/net/Uri", "parse", "(Ljava/lang/String;)Landroid/net/Uri;", uriString.object<jstring>());
		QAndroidIntent intent("android.intent.action.VIEW");
		intent.handle().callObjectMethod("setData", "(Landroid/net/Uri;)Landroid/content/Intent;", uri.object());
		QtAndroid::startActivity(intent.handle(), 0);
		/* If we get an error here, there is no app store on the phone */
		if (jniEnv->ExceptionCheck()) jniEnv->ExceptionClear();
	}
}

void Backend::shareRecipe(Recipe *recipe) {
	const QString idHex = QString::fromLatin1(recipe->getId().toHex()).toUpper();

	QAndroidIntent intent("android.intent.action.SEND");

	QAndroidJniObject metaType = QAndroidJniObject::fromString("openrecipes/recipe_xml");
	intent.handle().callObjectMethod("setType", "(Ljava/lang/String;)Landroid/content/Intent;", metaType.object<jstring>());

	QAndroidJniObject contentUriString = QAndroidJniObject::fromString(QString("content://org.jschwab.openrecipes.provider/recipe_xml/%1").arg(idHex));
	QAndroidJniObject contentUri = QAndroidJniObject::callStaticObjectMethod("android/net/Uri", "parse", "(Ljava/lang/String;)Landroid/net/Uri;", contentUriString.object<jstring>());
	QAndroidJniObject extraStream = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Intent", "EXTRA_STREAM");
	intent.handle().callObjectMethod("putExtra", "(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;", extraStream.object<jstring>(), contentUri.object());

	QAndroidJniObject title = QAndroidJniObject::fromString(tr("Share recipe"));
	QAndroidJniObject chooser = QAndroidJniObject::callStaticObjectMethod("android/content/Intent", "createChooser", "(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;", intent.handle().object(), title.object<jstring>());

	QtAndroid::startActivity(chooser, 0);
	QAndroidJniEnvironment jniEnv;
	if (jniEnv->ExceptionCheck()) jniEnv->ExceptionClear();
}
#else
bool Backend::importRecipeFromFile(const QString &path) {
	const QString resolvedPath = QUrl::fromUserInput(path, QDir::currentPath(), QUrl::AssumeLocalFile).toLocalFile();
	qDebug() << "Importing recipe from file" << resolvedPath;
	QFile f(resolvedPath);
	if (!f.open(QFile::ReadOnly | QFile::Text)) return false;
	QTextStream stream(&f);
	try {
		Recipe *r = new Recipe(stream.readAll(), nullptr);
		emit recipeImported(r->getId());
		delete r;
	} catch (const InternalError &e) {
		return false;
	}
	return true;
}

bool Backend::saveRecipeToFile(Recipe *recipe, const QString &path) {
	const QString resolvedPath = QUrl::fromUserInput(path, QDir::currentPath(), QUrl::AssumeLocalFile).toLocalFile();
	QFile f(resolvedPath);
	if (!f.open(QFile::WriteOnly | QFile::Text | QFile::Truncate)) return false;
	QTextStream stream(&f);
	stream << recipe->toXML();
	return true;
}
#endif

#ifdef Q_OS_WIN
void Backend::checkForUpdates() {
	qDebug() << "Backend Thread is" << QThread::currentThread();
	CheckForUpdateAsync *cfu = new CheckForUpdateAsync(this);
	connect(cfu, CheckForUpdateAsync::updateAvailable, [&](const QVersionNumber &version) {
			emit updateAvailable(version.toString());
	});
	connect(cfu, CheckForUpdateAsync::noUpdateAvailable, [&](bool suc) {
			qDebug() << "No Update (checking succeeded: " << suc << ")";
	});
	connect(cfu, QThread::finished, cfu, QObject::deleteLater);
	cfu->start();
}

void Backend::update(const QString &version) {
	UpdateAsync *u;
	try {
		u = new UpdateAsync(this, QVersionNumber::fromString(version));
	} catch (const InternalError &e) {
		emit updateDownloadFailed();
		return;
	}
	connect(u, UpdateAsync::downloadFailed, [&]() {
			emit updateDownloadFailed();
	});
	connect(u, QThread::finished, u, QObject::deleteLater);
	u->start();
}
#endif
