/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "WindowsSelfUpdate.h"
#include "InternalError.h"
#include "Globals.h"

#include <config.h>

#include <QApplication>
#include <fstream>
#include <QProcess>
#include <QString>
#include <QList>
#include <QByteArray>
#include <QtNetwork>
#include <sodium.h>

QString WindowsSelfUpdate::getLocalMsiFileName() {
	const QString dirPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	return QDir::cleanPath(dirPath + QDir::separator() + "update.msi");
}

void WindowsSelfUpdate::cleanup() {
	if (QFileInfo::exists(getLocalMsiFileName())) QFile::remove(getLocalMsiFileName());
}

QByteArray WindowsSelfUpdate::getFileContent(const QString &url) {
	QNetworkReply *reply = getReply(url);
	reply->deleteLater();
	return reply->readAll();
}

void WindowsSelfUpdate::getFile(const QString &url, const QString &saveTo) {
	QNetworkReply *reply = getReply(url);
	QFile file(saveTo);
	if (!file.open(QIODevice::NewOnly | QIODevice::WriteOnly)) throw IERROR("Can't open temporary file");
	qDebug() << "Saving to" << saveTo;
	file.write(reply->readAll());
	reply->deleteLater();
}

QNetworkReply* WindowsSelfUpdate::getReply(const QString &url) {
	qDebug() << "Requesting" << url;
	QNetworkAccessManager *networkManager = new QNetworkAccessManager;
	const QUrl u(url);
	const QNetworkRequest request(u);
	QNetworkReply *reply = networkManager->get(request);
	QEventLoop loop;
	QObject::connect(reply, QNetworkReply::finished, &loop, QEventLoop::quit);
	QObject::connect(reply, QNetworkReply::sslErrors, [&](){loop.quit(); throw IERROR("SSL Error");});
	if (!reply->isFinished()) loop.exec();
	networkManager->deleteLater();
	if (reply->error()) throw IERROR(QString("Download failed: %1").arg(reply->errorString()));
	return reply;
}

CheckForUpdateAsync::CheckForUpdateAsync(QObject *parent)
:
	QThread(parent)
{}

void CheckForUpdateAsync::run() {
	qDebug() << "Checking for updates...";

	QVersionNumber curVer;
	try {
		curVer = QVersionNumber::fromString(QString::fromLatin1(getFileContent("https://api.openrecipes.jschwab.org/currentClientVersion.txt")));
	} catch (const InternalError &e) {
		emit noUpdateAvailable(false);
		return;
	}

	if (curVer > QVersionNumber::fromString(config::version)) {
		emit updateAvailable(curVer);
	} else {
		emit noUpdateAvailable(true);
	}
}

UpdateAsync::UpdateAsync(QObject *parent, const QVersionNumber &version)
:
	QThread(parent),
	version(version)
{
	if (version < QVersionNumber::fromString(config::version)) throw IERROR("Won't downgrade");
}

void UpdateAsync::run() {
	qDebug() << "Starting updates...";

	cleanup();

	const QString msiPath = getLocalMsiFileName();
	QByteArray sig;

	try {
		const QByteArray clientInfo = getFileContent(QString("https://api.openrecipes.jschwab.org/windowsClientInfo/%1.txt").arg(version.toString()));
		const QList<QByteArray> clientInfoSplit = clientInfo.split('\n');
		if (clientInfoSplit.size() < 2) throw IERROR("Invalid version info file");
		const QString msiUrl = QString::fromLatin1(clientInfoSplit.at(0));
		sig = QByteArray::fromHex(clientInfoSplit.at(1));
		getFile(msiUrl, msiPath);
	} catch (const InternalError &e) {
		emit downloadFailed();
		return;
	}

	qDebug() << "Update dowload finished, checking signature...";

	std::ifstream msiFile(msiPath.toStdString(), std::ios::in | std::ios::binary);
	unsigned char chunk[1024];
	crypto_sign_state state; 
	crypto_sign_init(&state);
	while (msiFile.good()) {
		msiFile.read(reinterpret_cast<char*>(chunk), sizeof(chunk));
		crypto_sign_update(&state, chunk, msiFile.gcount());
	}

	assert(static_cast<size_t>(Globals::getUpdateSigningKey().size()) >= crypto_sign_PUBLICKEYBYTES);

	if (crypto_sign_final_verify(&state, reinterpret_cast<const unsigned char*>(sig.constData()), reinterpret_cast<const unsigned char*>(Globals::getUpdateSigningKey().constData())) != 0) {
		qDebug() << "Signature is invalid";
		emit downloadFailed();
		return;
	}

	qDebug() << "Signature looks fine, starting update";

	if (QProcess::startDetached(QString("msiexec.exe /i %1").arg(QDir::toNativeSeparators(msiPath)))) {
		QApplication::quit();
		return;
	}

	qDebug() << "Couldn't start update";

	emit downloadFailed();
	return;
}
