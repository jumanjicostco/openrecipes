/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QTcpSocket>

/*!
 * \brief Common interface for SecureConnection and SocketConnection.
 *
 * NetworkPackage needs to handle QTcpSocket and SecureConnection, but QObject derived classes can't be templates. So we need to take this approach.
 */
class Connection : public QObject {
	Q_OBJECT

	public:
		/*!
		 * \brief Constructor.
		 * \param parent passed to QObject
		 */
		explicit Connection(QObject *parent) : QObject(parent) {}

		/*!
		 * \brief Number of bytes available to read.
		 * \return number of bytes that are available to read
		 */
		virtual size_t bytesAvailable() const = 0;

		/*!
		 * \brief Read up to upToBytes bytes.
		 * \param upToBytes maximum number of bytes to read
		 * \return data
		 */
		virtual QByteArray read(size_t upToBytes) = 0;

	signals:
		/*!
		 * \brief Emitted when new data is ready to be read.
		 */
		void readyRead();

		/*!
		 * \brief Emitted when the connection has been closed.
		 */
		void disconnected();
};

/*!
 * \brief Wrapper for a QTcpSocket.
 */
class SocketConnection : public Connection {
	Q_OBJECT
	
	private:
		/*!
		 * \brief Socket to use.
		 */
		QTcpSocket *const socket;

	public:
		/*!
		 * \brief Constructor.
		 *
		 * The class doesn't take ownership of the socket. The caller is still responsible to delete it.
		 */
		SocketConnection(QObject *parent, QTcpSocket *socket)
		:
			Connection(parent),
			socket(socket)
	{
		connect(socket, &QTcpSocket::readyRead, this, [this]() {emit readyRead();});
		connect(socket, &QTcpSocket::disconnected, this, [this]() {emit disconnected();});
	}

		size_t bytesAvailable() const override {
			return socket->bytesAvailable();
		}

		QByteArray read(size_t upToBytes) override {
			return socket->read(upToBytes);
		}
};

#endif //CONNECTION_H
