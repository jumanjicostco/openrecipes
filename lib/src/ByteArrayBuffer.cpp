/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ByteArrayBuffer.h"

#include <cassert>
#include <cstring>

QByteArray ByteArrayBuffer::read(size_t upToBytes) {
	if (!data.empty() && static_cast<size_t>(data.front().size()) == upToBytes) {
		QByteArray d = data.front();
		data.pop_front();
		return d;
	}
	QByteArray b;
	b.reserve(upToBytes);
	std::list<QByteArray>::iterator i = data.begin();
	while (i != data.end()) {
		if (static_cast<size_t>(i->size()) <= upToBytes - b.size()) {
			b += *i;
			data.erase(i++);
		} else {
			const size_t toRead = upToBytes - b.size();
			assert(toRead < static_cast<size_t>(i->size()));
			b += i->left(toRead);
			memmove(i->data(), i->data() + toRead, i->size() - toRead);
			i->chop(toRead);
			break;
		}
	}
	b.squeeze();
	return b;
}

QByteArray ByteArrayBuffer::read() {
	return read(getSize());
}

size_t ByteArrayBuffer::getSize() const {
	size_t s = 0;
	for (const auto &d : data) s += d.size();
	return s;
}

void ByteArrayBuffer::operator+=(QByteArray &data) {
	this->data.emplace_back();
	this->data.back().swap(data);
}
