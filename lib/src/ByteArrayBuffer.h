/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BYTE_ARRAY_BUFFER_H
#define BYTE_ARRAY_BUFFER_H

#include <QByteArray>
#include <list>

/*!
 * \brief Class to merge QByteArrays evicently.
 */
class ByteArrayBuffer {
	private:
		/*!
		 * \brief The buffered data.
		 */
		std::list<QByteArray> data;

	public:
		/*!
		 * \brief Read data.
		 * \param upToBytes read up to this amaount of bytes
		 */
		QByteArray read(size_t upToBytes);

		/*!
		 * \brief Read all data.
		 */
		QByteArray read();

		/*!
		 * \brief Get the size of the data in the buffer.
		 */
		size_t getSize() const;

		/*!
		 * \brief Move data into the buffer.
		 * 
		 * The added QByteArray gets invalid!
		 */
		void operator+=(QByteArray &data);
};

#endif //BYTE_ARRAY_BUFFER_H
