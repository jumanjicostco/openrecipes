/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SqlBackendBase.h"
#include "InternalError.h"

#include <QSqlQuery>
#include <QThread>
#include <QSqlError>
#include <QDebug>
#include <sodium.h>

QSqlDatabase SqlBackendBase::getDatabase() {
	const QString connectionName = QString::number(reinterpret_cast<uintptr_t>(QThread::currentThread()), 16);
	if (QSqlDatabase::contains(connectionName)) {
		return QSqlDatabase::database(connectionName);
	} else {
		qDebug() << "Opening database connection " << connectionName;
		connect(QThread::currentThread(), &QThread::finished, [connectionName]() {
				qDebug() << "Closing database connection " << connectionName;
				QSqlDatabase::removeDatabase(connectionName);
				}
		);
		return addDatabase(connectionName);
	}
}

QSqlQuery SqlBackendBase::getQuery() {
	return QSqlQuery(getDatabase());
}

void SqlBackendBase::generateSyncPublicKeyPair() {
	qDebug() << "Generating sync keys";
	QByteArray publicKey, secretKey;
	publicKey.resize(crypto_sign_PUBLICKEYBYTES);
	secretKey.resize(crypto_sign_SECRETKEYBYTES);
	crypto_sign_keypair(reinterpret_cast<unsigned char*>(publicKey.data()), reinterpret_cast<unsigned char*>(secretKey.data()));
	QSqlQuery query = getQuery();
	query.prepare("INSERT INTO settings (name, value) VALUES "
			"(\"syncPublicKey\", :publicKey), (\"syncSecretKey\", :secretKey);");
	query.bindValue(":publicKey", publicKey);
	query.bindValue(":secretKey", secretKey);
	tryExec(query);
	sodium_memzero(secretKey.data(), secretKey.size());
}

void SqlBackendBase::tryExec(QSqlQuery &query) {
	if (!query.exec()) throw IERROR(QString("Can't execute query \"%1\": %2").arg(query.lastQuery()).arg(query.lastError().text()));
}

QVariant SqlBackendBase::getSetting(const QString &name) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT value FROM settings WHERE name = :name;");
	query.bindValue(":name", name);
	tryExec(query);
	if (!query.next()) throw SQLNORESULT();
	QVariant v = query.value(0);
	if (v.isNull()) throw SQLNORESULT();
	return v;
}

void SqlBackendBase::setSetting(const QString &name, const QVariant &value) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT COUNT(*) FROM settings WHERE name = :name;");
	query.bindValue(":name", name);
	tryExec(query);
	if (!query.next()) throw SQLNORESULT();
	if (query.value(0).toInt()) {
		query.prepare("UPDATE settings SET value = :value WHERE name = :name;");
	} else {
		query.prepare("INSERT INTO settings (name, value) VALUES (:name, :value);");
	}
	query.bindValue(":name", name);
	query.bindValue(":value", value);
	tryExec(query);
}

int SqlBackendBase::getDbSchema() {
	return getSetting("dbSchema").value<int>();
}

QByteArray SqlBackendBase::getSyncSecretKey() {
	return getSetting("syncSecretKey").value<QByteArray>();
}

QByteArray SqlBackendBase::getSyncPublicKey() {
	return getSetting("syncPublicKey").value<QByteArray>();
}

std::function<QSqlDatabase (const QString &connectionName)> SqlBackendBase::addDatabase = [](const QString&) -> QSqlDatabase {
	throw IERROR("This function must be replaced");
	assert(false);
	return QSqlDatabase();
};
