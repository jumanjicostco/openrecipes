TEMPLATE = lib
TARGET = openrecipes
CONFIG += staticlib
QT += core network sql
include( ../common.pri )

SOURCES +=  \
	src/ByteArrayBuffer.cpp \
	src/NetworkPackage.cpp \
	src/FixedSizeNetworkPackage.cpp \
	src/SqlBackendBase.cpp \
	src/SecureConnection.cpp

HEADERS +=  \
	src/ByteArrayBuffer.h \
	src/Connection.h \
	src/FixedSizeNetworkPackage.h \
	src/InternalError.h \
	src/NetworkPackage.h \
	src/SqlBackendBase.h \
	src/SecureConnection.h

QMAKE_SUBSTITUTES += \
	src/config.h.in
