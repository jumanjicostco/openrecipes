/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TCP_CONNECTION
#define TCP_CONNECTION

#include "HelperDefs.h"
#include "SecureServerConnection.h"

#include <QObject>
#include <memory>
#include <unordered_set>

class Request;
struct DbItem;

class TcpConnection : public QObject {
	Q_OBJECT

	private:
		SecureServerConnection *connection;
		const quint64 startupTime;
		bool loggedIn;
		bool requestInProgress;
		quint64 userId;

		static std::unordered_set<quint64> loggedInUsers;

		void write(const QByteArray &data);

	private slots:
		void onNewRequest(std::shared_ptr<Request> request);

	public:
		TcpConnection(QObject *parent, QTcpSocket *socket, quint64 startupTime);
		~TcpConnection();
		static size_t getNumberOfLoggedInUsers();

	public slots:
		void onUserLoggedIn(quint64 userId);
		void onGotIds(const GetIdsRes &res);
		void onAddedOrUpdated(bool succ);
		void onDeleted(bool succ);
		void onGotChanged(const GetChangedRes &res);
		void onGotSyncKey(const GetSyncKeyRes &res);
		void onSettedSyncKey(bool succ);

	signals:
		void closed();
};

#endif //TCP_CONNECTION
