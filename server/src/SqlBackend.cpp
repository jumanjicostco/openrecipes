/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SqlBackend.h"
#include "DbItem.h"

#include <InternalError.h>

#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <sodium.h>

QSqlDatabase SqlBackend::addDatabase(const QString &connectionName) {
	QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", connectionName);
	db.setConnectOptions("MYSQL_OPT_RECONNECT=1");
	db.setDatabaseName(conf.database);
	db.setUserName(conf.user);
	db.setPassword(conf.password);
	db.setHostName(conf.host);
	db.setPort(conf.port);
	if (!db.open()) throw IERROR("Can't open database");
	return db;
}

void SqlBackend::init(const SqlBackend::Config &conf) {
	SqlBackend::conf = conf;
	SqlBackendBase::addDatabase = addDatabase;

	cleanupTimer.reset(new QTimer);
	cleanupTimer->setSingleShot(false);
	cleanupTimer->setInterval(60 * 1000);
	connect(cleanupTimer.get(), &QTimer::timeout, []() {cleanup();});
	cleanupTimer->start();

	if (getDbSchema() > 1) throw IERROR("Can't handle db schema, please update");
	
	try {
		getSyncPublicKey();
		getSyncSecretKey();
	} catch (const SqlNoResult &e) {
		generateSyncPublicKeyPair();
	}
}

void SqlBackend::cleanup() {
	qDebug() << "Running db cleanup code";
	QSqlQuery query = getQuery();
	query.prepare("DELETE FROM syncKeys WHERE UNIX_TIMESTAMP(CURRENT_TIMESTAMP) - UNIX_TIMESTAMP(lastModified) > 1800;");
	try {
		tryExec(query);
	} catch (const InternalError &e) {
		qWarning() << "Can't execute cleanup query:" << e.what();
	}
}

quint64 SqlBackend::getUserId(const QByteArray &key) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT id FROM users "
			"WHERE publicKey = :key;");
	query.bindValue(":key", key);
	tryExec(query);

	if (!query.next()) {
		qDebug() << "New user";
		query.prepare("INSERT INTO users (publicKey, lastLogin) "
				"VALUES (:key, DATE(NOW()));");
		query.bindValue(":key", key);
		tryExec(query);
		query.prepare("SELECT LAST_INSERT_ID();");
		tryExec(query);
		if (!query.next()) throw IERROR("Can't get last id");
		return query.value(0).value<quint64>();
	}
	const quint64 id = query.value(0).value<quint64>();
	query.prepare("UPDATE users SET lastLogin = DATE(NOW()) "
			"WHERE id = :id;");
	query.bindValue(":id", id);
	query.bindValue(":date", id);
	return id;
}

std::vector<QByteArray> SqlBackend::getIds(quint64 userId) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT id FROM data "
			"WHERE userId = :userId;");
	query.bindValue(":userId", userId);
	tryExec(query);

	std::vector<QByteArray> ids;
	ids.reserve(query.size());
	while(query.next()) ids.push_back(query.value(0).value<QByteArray>());
	return ids;
}

void SqlBackend::addOrUpdate(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce, quint64 userId) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT COUNT(*) FROM data "
			"WHERE id = :id AND userId = :userId");
	query.bindValue(":id", id);
	query.bindValue(":userId", userId);
	tryExec(query);

	if (!query.next()) throw SQLNORESULT();
	const bool count = query.value(0).value<quint64>();

	if (count) {
		query.prepare("UPDATE data SET cipher = :cipher, nonce = :nonce "
				"WHERE id = :id AND userId = :userId");
		query.bindValue(":id", id);
		query.bindValue(":userId", userId);
		query.bindValue(":cipher", cipher);
		query.bindValue(":nonce", nonce);
		tryExec(query);
	} else {
		query.prepare("INSERT INTO data (id, userId, cipher, nonce) VALUES "
				"(:id, :userId, :cipher, :nonce)");
		query.bindValue(":id", id);
		query.bindValue(":userId", userId);
		query.bindValue(":cipher", cipher);
		query.bindValue(":nonce", nonce);
		tryExec(query);
	}
}

void SqlBackend::deleteEntry(const QByteArray &id, quint64 userId) {
	QSqlQuery query = getQuery();
	query.prepare("DELETE FROM data "
			"WHERE id = :id AND userId = :userId");
	query.bindValue(":id", id);
	query.bindValue(":userId", userId);
	tryExec(query);
}

std::vector<DbItem> SqlBackend::getChanged(quint64 timestamp, quint64 userId) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT id, cipher, nonce FROM data "
			"WHERE userId = :userId AND UNIX_TIMESTAMP(lastModified) > :timestamp;");
	query.bindValue(":timestamp", timestamp);
	query.bindValue(":userId", userId);
	tryExec(query);

	std::vector<DbItem> res;
	res.reserve(query.size());
	while(query.next()) res.emplace_back(
			query.value(0).value<QByteArray>(),
			query.value(1).value<QByteArray>(),
			query.value(2).value<QByteArray>()
			);
	return res;
}

std::pair<QByteArray, QByteArray> SqlBackend::getAndDelSyncKey(const QByteArray &id) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT cipher, nonce FROM syncKeys "
			"WHERE id = :id;");
	query.bindValue(":id", id);
	tryExec(query);

	if (!query.next()) throw SQLNORESULT();
	const QByteArray cipher = query.value(0).value<QByteArray>();
	const QByteArray nonce = query.value(1).value<QByteArray>();

	query.prepare("DELETE FROM syncKeys WHERE id = :id;");
	query.bindValue(":id", id);
	tryExec(query);

	return std::make_pair(cipher, nonce);
}

void SqlBackend::addSyncKey(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT COUNT(*) FROM syncKeys "
			"WHERE id = :id;");
	query.bindValue(":id", id);
	tryExec(query);
	if (!query.next()) throw SQLNORESULT();
	const bool idInDB = query.value(0).toBool();

	if (idInDB) {
		query.prepare("UPDATE syncKeys SET cipher = :cipher, nonce = :nonce "
				"WHERE id = :id;");
	} else {
		query.prepare("INSERT INTO syncKeys (id, cipher, nonce) "
				"VALUES (:id, :cipher, :nonce);");
	}
	query.bindValue(":id", id);
	query.bindValue(":cipher", cipher);
	query.bindValue(":nonce", nonce);
	tryExec(query);
}

unsigned long SqlBackend::getNumberOfTodaysUsers() {
	QSqlQuery query = getQuery();
	query.prepare("SELECT COUNT(*) FROM users "
			"WHERE lastLogin = DATE(NOW());");
	tryExec(query);
	if (!query.next()) throw SQLNORESULT();
	return query.value(0).value<unsigned long>();
}

std::unique_ptr<QTimer> SqlBackend::cleanupTimer;
SqlBackend::Config SqlBackend::conf;
